from django.test import TestCase, Client,  LiveServerTestCase
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
import time
from .views import story7
import os



# Create your tests here.

class Story7UnitTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.response = self.client.get('/story7/')
        self.page_content = self.response.content.decode('utf8')

    def test_story7_url_exists(self):
        self.assertEqual(self.response.status_code, 200)

    def test_story7_check_template_used(self):
        self.assertTemplateUsed(self.response, 'story7.html')
    
    def test_story7_check_function_used(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, story7)
    
    def test_story7_content_exists(self):
        self.assertIn('Activities', self.page_content)
        self.assertIn('Experiences', self.page_content)
        self.assertIn('Achievements', self.page_content)

class Story7FunctionalTest(LiveServerTestCase):

    def setUp(self):
        super(Story7FunctionalTest, self).setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            # self.browser = webdriver.Chrome()
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
    

    def tearDown(self):
        self.browser.quit()
        super(Story7FunctionalTest, self).tearDown()

    def test_website(self):
        self.browser.get(self.live_server_url + '/story7')

        #Check if accordion exists
        self.assertIn("+ Activities", self.browser.page_source)
        self.assertIn("+ Experience", self.browser.page_source)
        self.assertIn("+ Achievements", self.browser.page_source)
        self.assertIn('<div class="main-about', self.browser.page_source)
        self.assertIn('<div class="about-container', self.browser.page_source)
        self.assertIn('<div class="accordion-container', self.browser.page_source)
        self.assertIn('<div class="accordion', self.browser.page_source)
        time.sleep(3)

        #Check if accordion1's content exists
        self.browser.find_element_by_class_name('accordion1').click()
        self.assertIn("Saat ini, aku sedang menjalani semester 3 (atau tahun kedua) di Fasilkom UI.", self.browser.page_source)
        time.sleep(3)

        #Check if accordion2's content exists
        self.browser.find_element_by_class_name('accordion2').click()
        time.sleep(3)
        self.assertIn("Aku memulai kegiatan-kegiatan volunteer ini sejak SMA.", self.browser.page_source)
        time.sleep(3)

        #Check if accordion3's content exists
        self.browser.find_element_by_class_name('accordion3').click()
        time.sleep(3)
        self.assertIn("Sejujurnya, aku bukanlah orang yang memiliki banyak prestasi", self.browser.page_source)
        time.sleep(3)

        #Check if accordion4's content exists
        self.browser.find_element_by_class_name('accordion4').click()
        time.sleep(3)
        self.assertIn("Ada banyak sekali novel yang aku sukai.", self.browser.page_source)
        time.sleep(3)

        #Check if accordion5's content exists
        self.browser.find_element_by_class_name('accordion5').click()
        time.sleep(3)
        self.assertIn("Ngomongin K-Drama, gaakan ada abisnya deh!", self.browser.page_source)
        time.sleep(3)

        #Check theme switch
        self.browser.find_element_by_id('dot2').click()
        time.sleep(3)
        bg = self.browser.find_element_by_class_name('main-about').value_of_css_property('background-color')
        self.assertIn('rgba(194, 210, 255, 1)', bg)
        time.sleep(3)
        self.browser.find_element_by_id('dot1').click()
        time.sleep(3)
        bg = self.browser.find_element_by_class_name('main-about').value_of_css_property('background-color')
        self.assertIn('rgba(255, 209, 194, 1)', bg)



    
    def test_up_down_button(self):
        self.browser.get(self.live_server_url + '/story7')

        #Get buttons by path
        time.sleep(5)
        upBtn = self.browser.find_element_by_xpath("/html/body/div[3]/div/div[2]/div/div[1]/div[1]/div/div[1]")
        downBtn =  self.browser.find_element_by_xpath("/html/body/div[3]/div/div[2]/div/div[1]/div[1]/div/div[2]")
        
        #Check default order
        #Activities should appear before Experiences
        page_source = self.browser.page_source
        print("index of activities is: " + str(page_source.find('Activities')))
        print("index of experiences is: " + str(page_source.find('Experiences')))
        self.assertTrue(page_source.find('Activities') < page_source.find('Experiences'))

        #Check order when down button in "+ Activities" is clicked.
        #Activities should appear after Experiences
        downBtn.click()
        time.sleep(1)
        page_source = self.browser.page_source
        print("index of activities is: " + str(page_source.find('Activities')))
        print("index of experiences is: " + str(page_source.find('Experiences')))
        self.assertTrue(page_source.find('Activities') > page_source.find('Experiences'))

        #Check order when up button in "+ Activities" is clicked
        #Activities should appear before Experiences
        upBtn.click()
        time.sleep(1)
        page_source = self.browser.page_source
        print("index of activities is: " + str(page_source.find('Activities')))
        print("index of experiences is: " + str(page_source.find('Experiences')))
        self.assertTrue(page_source.find('Activities') < page_source.find('Experiences'))
