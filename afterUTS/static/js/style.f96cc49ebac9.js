// ============ JAVASCRIPT STORY 7 ==============

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}


$('.button-up').click(function(){
  var selectedCard = $(this).parent().parent().parent();
  selectedCard.insertBefore(selectedCard.prev());
  console.log("up")
})

$('.button-down').click(function(){
  var selectedCard = $(this).parent().parent().parent();
  selectedCard.insertAfter(selectedCard.next());
  console.log("down")
})
