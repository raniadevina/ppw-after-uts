from django.urls import path
from .views import story8, fungsidata

app_name = 'story8'
urlpatterns = [
    path('', story8, name='story8'),
    path('data/', fungsidata, name='fungsidata')
]
