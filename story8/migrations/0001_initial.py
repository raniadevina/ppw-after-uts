# Generated by Django 3.1.3 on 2020-11-30 08:58

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='LikedBook',
            fields=[
                ('publishedDate', models.CharField(max_length=200, null=True)),
                ('bookID', models.CharField(max_length=10, primary_key=True, serialize=False)),
                ('likes', models.IntegerField(default=0)),
                ('cover', models.TextField(blank=True)),
                ('title', models.CharField(max_length=100)),
                ('publisher', models.CharField(blank=True, max_length=255)),
                ('author', models.CharField(blank=True, max_length=255)),
            ],
        ),
    ]
